#!/bin/bash
# Get GPU temp for Nvidia
nvidia-smi | awk 'NR==10 {print $3}' | sed 's/C/°C/'
