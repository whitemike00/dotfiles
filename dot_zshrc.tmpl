# Path to your oh-my-zsh installation
export ZSH="$HOME/.oh-my-zsh"
export PATH="$HOME/.rbenv/bin:$PATH:$HOME/.local/bin:$HOME/go/bin:/usr/bin/bin"

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# Fix slowness of pastes with zsh-syntax-highlighting.zsh
pasteinit() {
  OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
  zle -N self-insert url-quote-magic
}

pastefinish() {
  zle -N self-insert $OLD_SELF_INSERT
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish
source ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
export ZSH_HIGHLIGHT_MAXLENGTH="60"

# ZSH Theme
ZSH_THEME="powerlevel10k/powerlevel10k"

# Uncomment the following line to use case-sensitive completion.
CASE_SENSITIVE="off"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="off"

# Uncomment the following line to automatically update without prompting.
DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
export UPDATE_ZSH_DAYS=15

# Uncomment the following line if pasting URLs and other text is messed up.
DISABLE_MAGIC_FUNCTIONS="true"

# Uncomment the following line to disable colors in ls.
DISABLE_LS_COLORS="off"

# Uncomment the following line to disable auto-setting terminal title.
DISABLE_AUTO_TITLE="off"

# Uncomment the following line to enable command auto-correction.
ENABLE_CORRECTION="false"

# Uncomment the following line to display red dots whilst waiting for completion.
COMPLETION_WAITING_DOTS="true"

# History time stamp
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
HIST_STAMPS="dd.mm.yyyy"

# ZSH plugins
plugins=(
git
zsh-autosuggestions
terraform
zsh-completions
zsh-syntax-highlighting
history)

source $ZSH/oh-my-zsh.sh

# Alias
if [[ -e ~/.zsh_aliases ]]; then
	source ~/.zsh_aliases
fi

# User configuration
export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR=vim
else
  export EDITOR=vim
fi

# Manpager
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
# Compilation flags
export ARCHFLAGS="-arch x86_64"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Kubectl use new auth
export USE_GKE_GCLOUD_AUTH_PLUGIN=True

# GCP Tokens
export GCP_PROD=/home/hmichael/Documents/gcp_tokens/pj-be-oria-prod-45c1932a72df.json
export GCP_PROD2=/home/hmichael/Documents/gcp_tokens/pj-be-oria-prod2-1e27f0ed4865.json
export GCP_QA=/home/hmichael/Documents/gcp_tokens/pj-be-oria-qa-413a063179af.json
export GCP_TESTING=/home/hmichael/Documents/gcp_tokens/pj-be-oria-testing-52b4913a62f1.json
export GCP_SANDBOX=/home/hmichael/Documents/gcp_tokens/pj-be-oria-sandbox-c1a6909bcb90.json
export GCP_GPV2_TESTING=/home/hmichael/Documents/gcp_tokens/pj-be-gpv2-testing-5b176e19b893.json
export GCP_GPV2_QA=/home/hmichael/Documents/gcp_tokens/pj-be-gpv2-qa2-824b2726034d.json
export GCP_GPV2_PROD=/home/hmichael/Documents/gcp_tokens/pj-be-gpv2-prod.json

eval "$(rbenv init - zsh)"
autoload -U compinit && compinit

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/hmichael/Downloads/google-cloud-sdk/path.zsh.inc' ]; then . '/home/hmichael/Downloads/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/hmichael/Downloads/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/hmichael/Downloads/google-cloud-sdk/completion.zsh.inc'; fi
