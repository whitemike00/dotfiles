#!/usr/bin/bash

# set US qwerty keyboard for X11 session
setxkbmap us

# replace value to switch workspace
sed -i 's/$mod+ampersand/$mod+1/' /home/hmichael/.config/i3/config
sed -i 's/$mod+eacute/$mod+2/' /home/hmichael/.config/i3/config
sed -i 's/$mod+quotedbl/$mod+3/' /home/hmichael/.config/i3/config
sed -i 's/$mod+apostrophe/$mod+4/' /home/hmichael/.config/i3/config
sed -i 's/$mod+parenleft/$mod+5/' /home/hmichael/.config/i3/config
sed -i 's/$mod+section/$mod+6/' /home/hmichael/.config/i3/config
sed -i 's/$mod+egrave/$mod+7/' /home/hmichael/.config/i3/config
sed -i 's/$mod+exclam/$mod+8/' /home/hmichael/.config/i3/config
sed -i 's/$mod+ccedilla/$mod+9/' /home/hmichael/.config/i3/config
sed -i 's/$mod+agrave/$mod+0/' /home/hmichael/.config/i3/config

# replace value to move focused container
sed -id 's/$mod+Shift+ampersand/$mod+Shift+1/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+eacute/$mod+Shift+2/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+quotedbl/$mod+Shift+3/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+apostrophe/$mod+Shift+4/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+5/$mod+Shift+5/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+section/$mod+Shift+6/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+egrave/$mod+Shift+7/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+exclam/$mod+Shift+8/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+ccedilla/$mod+Shift+9/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+agrave/$mod+Shift+0/' /home/hmichael/.config/i3/config
