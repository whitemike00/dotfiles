#!/usr/bin/bash

# set belgian keyboard for X11 session
setxkbmap be

# replace value to switch workspace
sed -i 's/$mod+1/$mod+ampersand/' /home/hmichael/.config/i3/config
sed -i 's/$mod+2/$mod+eacute/' /home/hmichael/.config/i3/config
sed -i 's/$mod+3/$mod+quotedbl/' /home/hmichael/.config/i3/config
sed -i 's/$mod+4/$mod+apostrophe/' /home/hmichael/.config/i3/config
sed -i 's/$mod+5/$mod+parenleft/' /home/hmichael/.config/i3/config
sed -i 's/$mod+6/$mod+section/' /home/hmichael/.config/i3/config
sed -i 's/$mod+7/$mod+egrave/' /home/hmichael/.config/i3/config
sed -i 's/$mod+8/$mod+exclam/' /home/hmichael/.config/i3/config
sed -i 's/$mod+9/$mod+ccedilla/' /home/hmichael/.config/i3/config
sed -i 's/$mod+0/$mod+agrave/' /home/hmichael/.config/i3/config

# replace value to move focused container
sed -id 's/$mod+Shift+1/$mod+Shift+ampersand/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+2/$mod+Shift+eacute/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+3/$mod+Shift+quotedbl/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+4/$mod+Shift+apostrophe/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+5/$mod+Shift+5/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+6/$mod+Shift+section/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+7/$mod+Shift+egrave/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+8/$mod+Shift+exclam/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+9/$mod+Shift+ccedilla/' /home/hmichael/.config/i3/config
sed -id 's/$mod+Shift+0/$mod+Shift+agrave/' /home/hmichael/.config/i3/config
